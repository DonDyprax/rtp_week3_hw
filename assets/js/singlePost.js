import { singleton } from "./singleton.js";
import { factory } from "./elementFactory.js";
import { getAuthor, getTag } from "./helpers.js";
import {
  postTitle,
  postSubtitle,
  postImageContainer,
  postAuthor,
  postBody,
  postDate,
  postLikes,
  postComments,
  userSelect,
  commentForm,
  userComment,
  postTags
} from "./consts.js";

const editButton = document.getElementById("btn-edit-post");
const deleteButton = document.getElementById("btn-delete-post");
const requestManager = singleton.getInstance();
const elementFactory = factory;

let post, comments;

window.onload = () => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const id = urlParams.get("id");

  (async () => {
    post = await requestManager.get("http://localhost:3000/posts/" + `${id}`);
    comments = await requestManager.get("http://localhost:3000/comments");
    setPostFields();
    filterPostComments();
    setComments();
    setUsers();
    setTags();
  })();
};

window.onunload = () => {
  const likeValue = parseInt(document.querySelector('.btn-like p').innerHTML);
  if(likeValue != post.likes) {

    let editedPost = {
      id: post.id,
      tags: post.tags,
      title: post.title,
      subTitle: post.subTitle,
      body: post.body,
      createDate: post.createDate,
      author: post.author,
      likes: likeValue,
      image: post.image
    };

    requestManager.put("http://localhost:3000/posts/" + post.id, editedPost);
  }
}

editButton.addEventListener("click", () => {
  window.location.href = `../../assets/views/editPost.html?id=${post.id}`
});

deleteButton.addEventListener('click', async () => {
  await requestManager.delete('http://localhost:3000/posts/' + post.id);
  window.location.href = '/';
});


function setPostFields() {
  postTitle.insertAdjacentHTML("afterbegin", `<h2>${post.title}</h2>`);
  postSubtitle.insertAdjacentHTML("afterbegin", `<h3>${post.subTitle}</h3>`);
  postImageContainer.insertAdjacentHTML(
    "afterbegin",
    `<img src="${post.image}"/>`
  );
  getAuthor(post.author).then((res) => {
    postAuthor.innerHTML = `<i class="fas fa-user"></i> ${res}`;
  });
  postBody.innerHTML = post.body;
  postLikes.insertAdjacentElement("afterbegin", createLikeButton(post.likes));
  postDate.innerHTML = `<i class="fas fa-calendar-alt"></i> ${post.createDate}`;
}


function createLikeButton(likes) {
  let btn = document.createElement("button");
  btn.classList.add("btn-like");
  btn.innerHTML = `<p>${likes}</p> <i class="fas fa-heart"></i>`;

  btn.addEventListener('click', () => {
    const likeValue = document.querySelector('.btn-like p');
    likeValue.innerHTML = parseInt(likeValue.innerHTML) + 1;
  })

  return btn;
}

function filterPostComments() {
  const filteredArray = comments.filter(function (comment) {
    return comment.postId == post.id;
  });

  comments = filteredArray;
}

function setComments() {
  if (comments.length == 0) {
    postComments.innerHTML = "This post has no comments yet.";
  } else {
    for (let i = 0; i < comments.length; i++) {
      let element = elementFactory.createElement(comments[i], "comment");
      postComments.appendChild(element);
    }
  }
}

commentForm.addEventListener("submit", (ev) => {
  addNewComment(ev);
});


async function setUsers() {
  let users = await requestManager.get("http://localhost:3000/users");

  for (let i = 0; i < users.length; i++) {
    let option = document.createElement("option");
    option.setAttribute("value", `${users[i].id}`);
    option.appendChild(
      document.createTextNode(`${users[i].name} ${users[i].lastName}`)
    );
    userSelect.appendChild(option);
  }
}


async function addNewComment(ev) {
  ev.preventDefault();
  const allComments = await requestManager.get(
    "http://localhost:3000/comments"
  );
  const maxId = Math.max.apply(
    Math,
    allComments.map(function (obj) {
      return obj.id;
    })
  );

  const comment = {
    id: maxId + 1,
    comment: userComment.value,
    postId: post.id,
    user: parseInt(userSelect.value),
  };

  const newComment = elementFactory.createElement(comment, "comment");
  postComments.appendChild(newComment);

  requestManager.post("http://localhost:3000/comments", comment);

  console.log(comment);
}

async function setTags() {
  const tagIds = [...post.tags];
  let tags = [];

  for(let i = 0; i < tagIds.length; i++) {
    let tag = await requestManager.get("http://localhost:3000/tags/" + tagIds[i]);
    tags.push(tag.name);
  }

  for(let j = 0; j < tags.length; j++) {
    let tag = document.createElement('p');

    if(j == tags.length - 1) {
      tag.innerHTML = tags[j];
    } else {
      tag.innerHTML = `${tags[j]}, `;
    }

    postTags.appendChild(tag);
  }
}