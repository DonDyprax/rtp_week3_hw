import { singleton } from "./singleton.js";

const title = document.getElementById("title");
const subtitle = document.getElementById("subtitle");
const postBody = document.getElementById("postBody");
const date = document.getElementById("creationDate");
const author = document.getElementById("author");
const tagsContainer = document.querySelector(".tags-container");
const postForm = document.getElementById("post-form");

const requestManager = singleton.getInstance();

let post;

window.onload = () => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const id = urlParams.get("id");

  (async () => {
    post = await requestManager.get("http://localhost:3000/posts/" + `${id}`);
    setTags();
    setAuthors();
    setPostData();
  })();
};

postForm.addEventListener("submit", (ev) => {
  updatePost(ev);
});

async function updatePost(ev) {
  ev.preventDefault();

  let editedPost = {
    id: post.id,
    tags: getSelectedTags(),
    title: title.value,
    subTitle: subtitle.value,
    body: postBody.value,
    createDate: date.value.replaceAll("-", "/"),
    author: parseInt(author.value),
    likes: post.likes,
    image: image.value,
  };

  await requestManager.put("http://localhost:3000/posts/" + post.id, editedPost);
  window.location.href = `../../assets/views/singlePost.html?id=${post.id}`;
}

function setPostData() {
  title.value = post.title;
  subtitle.value = post.subTitle;
  postBody.value = post.body;
  image.value = post.image;
  date.value = post.createDate.replaceAll("/", "-");
}

async function setAuthors() {
  const authors = await requestManager.get("http://localhost:3000/authors");

  for (let i = 0; i < authors.length; i++) {
    let option = document.createElement("option");
    option.value = authors[i].id;
    option.appendChild(
      document.createTextNode(`${authors[i].name} ${authors[i].lastName}`)
    );
    author.appendChild(option);
  }

  author.value = post.author; //Setting the author's value
}

async function setTags() {
  let tags = await requestManager.get("http://localhost:3000/tags");

  for (let i = 0; i < tags.length; i++) {
    let container = document.createElement("div");

    let tag = document.createElement("input");
    tag.setAttribute("type", "checkbox");
    tag.setAttribute("name", "tags");
    tag.setAttribute("value", `${tags[i].id}`);
    tag.setAttribute("id", tags[i].slug);

    let label = document.createElement("label");
    label.setAttribute("for", tags[i].slug);
    label.innerHTML = tags[i].name;

    container.appendChild(label);
    container.appendChild(tag);
    tagsContainer.appendChild(container);
  }

  const allCheckboxes = document.getElementsByName("tags");
  for (let checkbox of allCheckboxes) {
    console.log(checkbox);
    if (post.tags.includes(parseInt(checkbox.value))) {
      checkbox.checked = true;
    }
  }
}

function getSelectedTags() {
  let selected = [];
  const allCheckboxes = document.getElementsByName("tags");
  for (let checkbox of allCheckboxes) {
    if (checkbox.checked) {
      selected.push(parseInt(checkbox.value));
    }
  }

  return selected;
}
