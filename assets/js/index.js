import { singleton } from "./singleton.js";
import { factory } from "./elementFactory.js";
import { getTag } from "./helpers.js";

const filtersContainer = document.querySelector(".filters__tags");

let posts;

const requestManager = singleton.getInstance();
const elementFactory = factory;

window.onload = () => {
  (async () => {
    posts = await requestManager.get("http://localhost:3000/posts");
    setFeaturedPosts();
    setRemainingPosts();
    setAllPosts();
    getFilters();
  })();
};

function setFeaturedPosts() {
  const featured = posts.slice(-3);
  const featuredContainer = document.querySelector(".featured-posts__posts");

  for (let i = 2; i >= 0; i--) {
    let post = elementFactory.createElement(featured[i], "post");
    featuredContainer.appendChild(post);
  }
}

function setRemainingPosts() {
  const remaining = posts.slice(0, -3);
  const remainingContainer = document.querySelector(".remaining-posts__posts");

  for (let i = 0; i < remaining.length; i++) {
    let post = elementFactory.createElement(remaining[i], "post");
    remainingContainer.appendChild(post);
  }
}

function setAllPosts() {
  const allContainer = document.querySelector(".all-posts__posts");

  for (let i = posts.length - 1; i >= 0; i--) {
    let post = elementFactory.createElement(posts[i], "post");
    allContainer.appendChild(post);
  }
}

async function getFilters() {
  let filters = await requestManager.get("http://localhost:3000/tags");

  function createTag(name) {
    let btn = document.createElement("button");
    btn.innerHTML = `<i class="fas fa-tags"></i> ${name}`;
    btn.onclick = function () {
      setFilters(name);
    };

    return btn;
  }

  filtersContainer.appendChild(createTag("All"));

  for (let i = 0; i < filters.length; i++) {
    let tag = createTag(filters[i].name);
    filtersContainer.appendChild(tag);
  }
}

function setFilters(tag) {
  const featuredPosts = document.querySelector(".featured-posts");
  const remainingPosts = document.querySelector(".remaining-posts");
  const allPosts = document.querySelector(".all-posts");
  const filteredPosts = document.querySelector(".filtered-posts");

  featuredPosts.innerHTML = "";
  remainingPosts.innerHTML = "";
  allPosts.innerHTML = "";
  filteredPosts.innerHTML = "";

  if (tag == "All") {
    const featuredHeading = document.createElement("h2");
    featuredHeading.innerHTML = "Featured Posts";
    featuredPosts.appendChild(featuredHeading);

    const featuredDiv = document.createElement("div");
    featuredDiv.classList.add("featured-posts__posts");

    console.log(featuredDiv);
    featuredPosts.appendChild(featuredDiv);
    setFeaturedPosts();

    const remainingHeading = document.createElement("h2");
    remainingHeading.innerHTML = "Remaining Posts";
    remainingPosts.appendChild(remainingHeading);

    const remainingDiv = document.createElement("div");
    remainingDiv.classList.add("remaining-posts__posts");

    remainingPosts.appendChild(remainingDiv);

    const allHeading = document.createElement("h2");
    allHeading.innerHTML = "All Posts";
    allPosts.appendChild(allHeading);

    const allDiv = document.createElement("div");
    allDiv.classList.add("all-posts__posts");

    allPosts.appendChild(allDiv);

    setRemainingPosts();
    setAllPosts();
  } else {
    setFilteredPosts(tag);
  }
}

function setFilteredPosts(filter) {
  const filteredPosts = document.querySelector(".filtered-posts");

  for (let i = 0; i < posts.length; i++) {
    for (let j = 0; j < posts[i].tags.length; j++) {
      getTag(posts[i].tags[j]).then((res) => {
        if (res == filter) {
          let post = elementFactory.createElement(posts[i], "post");
          filteredPosts.appendChild(post);
        }
      });
    }
  }

}
