import { singleton } from "./singleton.js";

const title = document.getElementById('title');
const subtitle = document.getElementById('subtitle');
const postBody = document.getElementById('postBody');
const date = document.getElementById('creationDate');
const author = document.getElementById('author');
const postForm = document.getElementById("post-form");
const tagsContainer = document.querySelector(".tags-container");

const requestManager = singleton.getInstance();

window.onload = () => {
  (async () => {
    setTags();
    setAuthors();
  })();
};

postForm.addEventListener("submit", (ev) => {
  addNewPost(ev);
});

async function addNewPost(ev) {
  ev.preventDefault();
  const allPosts = await requestManager.get("http://localhost:3000/posts");
  const maxId = Math.max.apply(Math, allPosts.map(function(obj) { return obj.id; }));

  let post = {
      "id": maxId + 1,
      "tags": getSelectedTags(),
      "title": title.value,
      "subTitle": subtitle.value,
      "body": postBody.value,
      "createDate": date.value.replaceAll("-","/"),
      "author": parseInt(author.value),
      "likes": 0,
      "image": image.value
  }

  console.log(post);
  await requestManager.post('http://localhost:3000/posts', post);
  window.location.href = '/';
}

async function setAuthors() {
    const authors  = await requestManager.get('http://localhost:3000/authors');

    for(let i = 0; i < authors.length; i++) {
        let option = document.createElement('option');
        option.value = authors[i].id;
        option.appendChild(document.createTextNode(`${authors[i].name} ${authors[i].lastName}`));
        author.appendChild(option);
    }
}

async function setTags() {
  let tags = await requestManager.get("http://localhost:3000/tags");

  for (let i = 0; i < tags.length; i++) {
    let container = document.createElement("div");

    let tag = document.createElement("input");
    tag.setAttribute("type", "checkbox");
    tag.setAttribute("name", "tags");
    tag.setAttribute("value", `${tags[i].id}`);
    tag.setAttribute("id", tags[i].slug);

    let label = document.createElement("label");
    label.setAttribute("for", tags[i].slug);
    label.innerHTML = tags[i].name;

    container.appendChild(label);
    container.appendChild(tag);
    tagsContainer.appendChild(container);
  }
}

function getSelectedTags() {
    let selected  = [];
    const allCheckboxes = document.getElementsByName('tags');
    for(let checkbox of allCheckboxes) {
      if(checkbox.checked) {
          selected.push(parseInt(checkbox.value));
      }
    }

    return selected;
}