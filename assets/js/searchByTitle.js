import { singleton } from "./singleton.js";
import { factory } from "./elementFactory.js";

const searchInput = document.getElementById("search-input");
const searchForm = document.getElementById('search-form');
const searchResults = document.querySelector('.search-results');
const requestManager = singleton.getInstance();

const elementFactory = factory;

window.onload = () => {
  (async () => {
  })();
};

searchForm.addEventListener('submit', event => setMatchingPosts(event));

async function filterPostByTitle() {
  const posts = await requestManager.get("http://localhost:3000/posts");

  const filteredArray = posts.filter(function (post) {
    return post.title.toLowerCase() == searchInput.value.toLowerCase();
  });

  return filteredArray;
}

async function setMatchingPosts(event) {
    event.preventDefault();

    let posts = await filterPostByTitle();

    for (let i = 0; i < posts.length; i++) {
        let post = elementFactory.createElement(posts[i], "post");
        searchResults.appendChild(post);
      }

    console.log(posts);
}
