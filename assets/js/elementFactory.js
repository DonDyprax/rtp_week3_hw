import { getUser } from "./helpers.js";

function ElementFactory() {
  this.createElement = function (data, type) {
    let element;

    if (type === "post") {
      element = new Post(data);
    }

    if (type === "comment") {
      element = new Comment(data);
    }

    return element;
  };
}
/* This is the post's model. The resulting element will look like the following:

        <div class="post">
            <div class="post__image">
                <img src="https://i.ytimg.com/vi/INk1W8OujQI/maxresdefault.jpg" />
            </div>
            <div class="post__body">
                <div class="post__title">Mario</div>
                <div class="post__info">
                    <div class="post__likes">125</div>
                    <div class="post__date">12/12/12</div>
                </div>
            </div>
        </div>
*/
const Post = function (data) {
  //This will be the main container (div) for the post's elements
  let postDiv = document.createElement("div");
  postDiv.classList.add("post");

  let postImage = document.createElement("div");
  postImage.classList.add("post__image");
  postImage.insertAdjacentHTML("afterbegin", `<img src=${data.image} />`);
  postDiv.appendChild(postImage);

  let postBody = document.createElement("div");
  postBody.classList.add("post__body");

  let postTitle = document.createElement("div");
  postTitle.classList.add("post__title");
  postTitle.innerHTML = data.title;

  let postInfo = document.createElement("div");
  postInfo.classList.add("post__info");

  let postLikes = document.createElement("div");
  postLikes.classList.add("post__likes");
  postLikes.innerHTML = `${data.likes} <i class="fas fa-heart"></i>`;

  let postDate = document.createElement("div");
  postDate.classList.add("post__date");
  postDate.innerHTML = `<i class="fas fa-calendar-alt"></i> ${data.createDate}`;

  postInfo.appendChild(postLikes);
  postInfo.appendChild(postDate);

  postBody.appendChild(postTitle);
  postBody.appendChild(postInfo);

  postDiv.appendChild(postBody);

  postDiv.addEventListener("click", () => {
    window.location.href = `../../assets/views/singlePost.html?id=${data.id}`;
  });

  return postDiv;
};

const Comment = function(data) {
  let commentDiv = document.createElement("div");
  commentDiv.classList.add("comment");

  let commentAuthor = document.createElement("div");
  commentAuthor.classList.add("comment__author");
  getUser(data.user).then( res => { commentAuthor.innerHTML = "@" + res});

  let commentBody = document.createElement("div");
  commentBody.classList.add("comment__body");
  commentBody.innerHTML = data.comment;

  commentDiv.appendChild(commentAuthor);
  commentDiv.appendChild(commentBody);

  return commentDiv;
}

export const factory = new ElementFactory();
