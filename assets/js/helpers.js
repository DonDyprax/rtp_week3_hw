import { singleton } from "./singleton.js"

const requestManager = singleton.getInstance();

export const getAuthor = async (id) => {
  let author = await requestManager.get("http://localhost:3000/authors/" + id);
  return `${author.name} ${author.lastName}`;
};

export const getUser = async (id) => {
    let user = await requestManager.get("http://localhost:3000/users/" + id);
    return `${user.name} ${user.lastName}`;
}

export const getTag = async (id) => {
  let tag = await requestManager.get("http://localhost:3000/tags/" + id);
  return tag.name;
}
