class RequestManager {
  async get(url) {
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      return await data;
    } else {
      return false;
    }
  }

  async post(url, data) {
    const request = await fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .catch((error) => console.error("Error:", error))
      .then((response) => console.log("Success:", response));
  }

  async delete(url) {
    const request = await fetch(url, {
      method: "DELETE",
      body: null,
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .catch((error) => console.error("Error:", error))
      .then((response) => console.log("Success:", response));
  }

  async put(url, data) {
    const request = await fetch(url, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .catch((error) => console.error("Error:", error))
      .then((response) => console.log("Success:", response));
  }
}

export const singleton = (function () {
  let instance;

  function createInstance() {
    let object = new RequestManager();
    return object;
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    },
  };
})();
